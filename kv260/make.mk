ts-firmware-kv260 := out/kv260/trs/ImageA.bin
ts-firmware-kv260-gz := out/kv260/trs/ImageA.bin.gz
ts-firmware-kv260-url := https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/ImageA.bin.gz?job=build-meta-ts-zynqmp-kria-starter

$(ts-firmware-kv260): $(ts-firmware-kv260-gz)
	@$(quiet) "  GUNZIP $@"
	${q}gunzip -c $< >$@

$(ts-firmware-kv260-gz): | $(builder-image)
	@$(quiet) "  CURL    $@"
	$(call run-builder,curl --create-dirs -L -o $@ $(ts-firmware-kv260-url))

kv260-trs-images-download: $(ts-firmware-kv260) $(rootfs-trs)

kv260-trs-firmware-flash:
	${q}echo
	${q}echo "The KV260 firmware cannot be flashed using a makefile target"
	${q}echo "Please use the board's built-in HTTP server to upload $(ts-firmware-kv260)"
	${q}echo "To do this, apply power to the board while holding down the FWUEN switch"
	${q}echo "then connect to http://192.168.0.111 and use the interface to upload the"
	${q}echo "firmware file to either slot A or B"
	${q}false

kv260-trs-rootfs-flash: trs-rootfs-flash


# Instructions for manual install of Xilinx firmware and Ubuntu OS on the
# KV260 are available at:
# https://www.xilinx.com/products/som/kria/kv260-vision-starter-kit/kv260-getting-started-ubuntu/setting-up-the-sd-card-image.html
#
# - The firmware cannot be downloaded without an AMD/Xilinx account.
#   Therefore you need to follow the instructions to install it.
#   Once you have downloaded the firmware binary, power up the board
#   while keeping the FWUEN button pressed. It will start an HTTP
#   server on http://192.168.0.111/ which you can use to update the
#   firmware in slot A or B.
#
# - The Ubuntu image can be downloaded and written to a USB disk like so:
#
#   make kv260-ubuntu-image-download
#   make kv260-ubuntu-image-flash DEV=/dev/sdX

ubuntu-kv260 = out/kv260/ubuntu/iot-limerick-kria-classic-desktop-2204-x06-20220614-78.img.xz
ubuntu-kv260-url = https://people.canonical.com/~platform/images/xilinx/kria-ubuntu-22.04/iot-limerick-kria-classic-desktop-2204-x06-20220614-78.img.xz

$(ubuntu-kv260): | $(builder-image)
	@$(quiet) "  CURL    $@"
	$(call run-builder,curl --create-dirs -L -o $@ $(rootfs-kv260-url))

kv260-ubuntu-image-download: $(ubuntu-kv260)

# growpart is in Ubuntu package cloud-guest-utils
kv260-ubuntu-image-flash:
	@if [ ! "$(DEV)" ]; then \
		echo 'Usage: make $@ DEV=<device path> (such as /dev/sdb)'; \
		false; \
	fi
	sudo sh -c "xzcat $(ubuntu-kv260) >$(DEV)"
	sudo sync $(DEV)
	sudo umount $(DEV)2 || :
	sudo growpart $(DEV) 2
	sudo sync $(DEV)
