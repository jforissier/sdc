all: cloud_agent-image-save

.PHONY: cloud_agent

cloud_agent: out/.stamp_cloud_agent

cloud_agent-image-push: FORCE out/.stamp_cloud_agent | $(builder-image)
	$(call run-builder,docker tag sc-cloud_agent $(DOCKER_NAMESPACE)/sc-cloud_agent)
	$(call run-builder,docker push $(DOCKER_NAMESPACE)/sc-cloud_agent)

images-push: cloud_agent-image-push

cloud_agent-image-save: out/sc-cloud_agent.tar.gz

out/.stamp_cloud_agent: cloud_agent/config/service_config.json \
			cloud_agent/Dockerfile \
			$(shell find cloud_agent/src -type f) | host-aarch64-registered $(builder-image)
	$(call run-builder,docker buildx build $(DOCKER_BUILD_OPTS) --platform aarch64 -t sc-cloud_agent cloud_agent)
	$(q)touch $@

_make_service_config_json:
	@$(quiet) "  GEN     cloud_agent/config/service_config.json"
	$(q)export _AWS_IOT_ENDPOINT=$$(aws iot describe-endpoint --endpoint-type iot:Data-ATS | grep endpointAddress | sed 's/.*: "\(.*\)".*/\1/'); \
		awk -f subst.awk <cloud_agent/config/service_config.json.in >cloud_agent/config/service_config.json

cloud_agent/config/service_config.json: cloud_agent/config/service_config.json.in | $(builder-image)
	$(q)$(call make-in-builder,_make_service_config_json)

out/sc-cloud_agent.tar.gz: out/.stamp_cloud_agent | $(builder-image)
	@$(quiet) "  DOCKSAV $@"
	$(q)$(call run-builder,bash -c 'docker save sc-cloud_agent | pigz >$@')

staging-gg-ca = out/staging/greengrassv2/SmartCameraCloudAgent

greengrass-cloud_agent-component: $(staging-gg-ca)/zip-build/SmartCameraCloudAgent.zip

$(staging-gg-ca)/zip-build/SmartCameraCloudAgent.zip: $(staging-gg-ca)/sc-cloud_agent.tar.gz \
				$(staging-gg-ca)/gdk-config.json \
				$(staging-gg-ca)/recipe.yaml | $(builder-image)
	@$(quiet) "  BUILD   $@"
	$(q)$(call run-builder,bash -c 'cd $(staging-gg-ca) && gdk component build')

$(staging-gg-ca)/sc-cloud_agent.tar.gz: out/sc-cloud_agent.tar.gz
	$(call cp-file)

$(staging-gg-ca)/gdk-config.json: cloud_agent/greengrassv2/SmartCameraCloudAgent/gdk-config.json
	$(call cp-file)

$(staging-gg-ca)/recipe.yaml: cloud_agent/greengrassv2/SmartCameraCloudAgent/recipe.yaml
	$(call cp-file)

greengrass-cloud_agent-component-publish: $(staging-gg-ca)/zip-build/SmartCameraCloudAgent.zip \
				out/.stamp_greengrass-attach-role-policy | $(builder-image)
	@$(quiet) "  PUBLISH $(notdir $<)"
	$(q)$(call run-builder,./aws/gdk_component_publish.sh $(staging-gg-ca))

greengrass-components: greengrass-cloud_agent-component

greengrass-components-publish: greengrass-cloud_agent-component-publish
