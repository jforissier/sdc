t := training

.PHONY: training

ifeq ($(TRAINING_MODE),off)

training:
	@echo "Info: training is disabled (TRAINING_MODE=$(TRAINING_MODE))"

else ifeq ($(TRAINING_MODE),local)

include $(t)/local.mk

else ifeq ($(TRAINING_MODE),aws)

include $(t)/aws.mk

else

$(error Invalid value for TRAINING_MODE)

endif # TRAINING_MODE
