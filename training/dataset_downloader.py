from dataset_downloader.coco_image_downloader import download_images_from_coco
from dataset_downloader.merge_coco_annotations import merge_coco_annotations
from dataset_downloader.annotation import annotate

classes = ["person", "car", "bus", "truck", "bicycle"]
download_images_from_coco(classes)
merge_coco_annotations()
annotate()
