
def ArgConf(ap):
    ap.add_argument("-ip", "--ipaddr", default="127.0.0.1",
                    help="IP address of the inference container")
    ap.add_argument("-p", "--inf_port", default="8080",
                    help="Port number of the inference container port number")
    ap.add_argument("-csp", "--cloud_address", default="",
                    help="IP address and port number of the cloud agent IP, "
                    "e.g., 127.0.0.1:8081")
    ap.add_argument("-csp_s", "--cloud_rtmp_stream", default="",
                    help="RTMP pull url for fetching the stream")
    ap.add_argument("-c", "--cam_path", help="Camera device Path")
    ap.add_argument("-r", "--rtsp", default="", help="RTSP stream link")
    ap.add_argument("-m", "--model", required=True,
                    help="AI model type (yolov3 or tiny_yolov3)")
    ap.add_argument("-nui", "--disable_ui", action="store_false",
                    help="Disable Web UI integration")
    ap.add_argument("-cloud", "--cloud_service",
                    help="Cloud Service Provider: aws or alibaba")
    args = vars(ap.parse_args())
    return args
