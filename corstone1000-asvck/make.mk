COREIMG_ZIP = corstone1000-asvck-1.0.5.coreimg.zip

o := out/corstone1000-asvck
s := corstone1000-asvck

.DELETE_ON_ERROR:

corstone1000-asvck-avh-image: $(o)/corstone1000-asvck-sdc.zip

$(o)/corstone1000-asvck-sdc.zip: $(o)/Info.plist $(o)/loaderfile $(o)/flash $(o)/seflash $(o)/kernel $(o)/sdcard $(o)/devicetree
	(cd $(o) && zip $(@F) $(^F))

$(o)/$(COREIMG_ZIP):
	mkdir -p $(o)
	wget -O $@ https://firmwares-us-east-1-avh-s3-arm-com.s3.amazonaws.com/corstone1000-asvck-1.0.5.coreimg-7f164faa-c065-45a2-8077-4686a76a6042

define extract-recipe
$(if $(2),$(o)/$(2),$(o)/$(1)): $(o)/$(COREIMG_ZIP)
	unzip -p $(o)/$(COREIMG_ZIP) $(1) >$(if $(2),$(o)/$(2),$(o)/$(1))
endef

$(eval $(call extract-recipe,Info.plist))
$(eval $(call extract-recipe,loaderfile))
$(eval $(call extract-recipe,flash))
$(eval $(call extract-recipe,seflash))
$(eval $(call extract-recipe,sdcard))
$(eval $(call extract-recipe,devicetree,devicetree-original))

$(o)/devicetree: $(o)/devicetree.dts
	dtc -q -I dts -O dtb $< >$@

$(o)/devicetree.dts: $(o)/devicetree-original.dts $(s)/devicetree.patch
	cp $< $@
	(cd $(o) && patch -p0 <../../corstone1000-asvck/devicetree.patch)

$(o)/devicetree-original.dts: $(o)/devicetree-original
	dtc -q -I dtb -O dts $< >$@

$(o)/kernel: $(o)/buildroot/images/Image
	cp $< $@

# Note: the procedure to create or modify corstone1000-asvck/br2_defconfig is as follows.
# 1.   mkdir -p out/corstone1000-asvck/buildroot && cd buildroot
# To ignore the current corstone1000-asvck/br2_defconfig and use aarch64_efi_defconfig instead
# (re-starting from scratch):
# 2.a. make O=../out/corstone1000-asvck/buildroot aarch64_efi_defconfig
# Or to use the current corstone1000-asvck/br2_defconfig:
# 2.b. make O=../out/corstone1000-asvck/buildroot defconfig DEFCONFIG=../corstone1000-asvck/br2_defconfig
# 3.   cd ../out/corstone1000-asvck/buildroot
#      make menuconfig
# 4. make savedefconfig DEFCONFIG=../corstone1000-asvck/br2_defconfig
$(o)/buildroot/.config: corstone1000-asvck/br2_defconfig
	mkdir -p $(@D)
	$(MAKE) -C buildroot defconfig DEFCONFIG=../$< O=../$(@D)

define br-env
PATH=$(PATH) HOME=$(HOME) USER=$(USER) SSH_AUTH_SOCK=$(SSH_AUTH_SOCK)
endef

corstone1000-asvck-kernel: $(o)/buildroot/images/Image

$(o)/buildroot/images/Image: $(o)/buildroot/.config
	cd $(o)/buildroot && env -i $(br-env) $(MAKE)

corstone1000-asvck-clean:
	(cd $(o) && rm -f corstone1000-asvck-sdc.zip Info.plist loaderfile flash seflash kernel sdcard devicetree-original devicetree-original.dts devicetree.dts devicetree)

corstone1000-asvck-cleaner: corstone1000-asvck-clean
	rm -f $(o)/$(COREIMG_ZIP)

clean: corstone1000-asvck-clean

cleaner: corstone1000-asvck-cleaner
