FROM python:3.7-slim-buster

RUN apt update
RUN apt upgrade -y
RUN apt install -y --no-install-recommends \
	build-essential \
	clang \
	cmake \
	gcc \
	gcc-aarch64-linux-gnu \
	g++-aarch64-linux-gnu \
	git \
	libedit-dev \
	libgl1-mesa-glx \
	libsm6 \
	libssl-dev \
	libtinfo-dev \
	libxext6 \
	libxml2-dev \
	llvm \
	llvm-dev \
	ocl-icd-opencl-dev \
	python3 \
	python3-dev \
	python3-pip \
	python3-setuptools \
	python3-wheel \
	wget \
	zlib1g-dev

#RUN apt install -y ocl-icd-opencl-dev
#RUN apt-get install gpg
#RUN wget -O - https://apt.kitware.com/keys/kitware-archive-latest.asc 2>/dev/null | gpg --dearmor - | tee /usr/share/keyrings/kitware-archive-keyring.gpg >/dev/null
#RUN echo 'deb [signed-by=/usr/share/keyrings/kitware-archive-keyring.gpg] https://apt.kitware.com/ubuntu/ bionic main' | tee /etc/apt/sources.list.d/kitware.list >/dev/null
#RUN apt-get update && rm /usr/share/keyrings/kitware-archive-keyring.gpg 
#RUN apt-get install -y kitware-archive-keyring && apt-get install cmake ocl-icd-opencl-dev -y

WORKDIR /
RUN git clone --recursive -b v0.10.dev0 https://github.com/apache/tvm.git
RUN mkdir /tvm/build
RUN cp tvm/cmake/config.cmake /tvm/build
RUN echo "set(USE_LLVM ON)" >> /tvm/build/config.cmake

WORKDIR /tvm/build
RUN cmake ..

RUN make -j$(nproc) install
ENV PYTHONPATH=/tvm/python:/tvm/topi/python:${PYTHONPATH}

COPY local/requirements.txt requirements.txt
RUN python3 -m pip install --upgrade pip
RUN python3 -m pip install -r requirements.txt

WORKDIR /
COPY src/ src/
COPY main.py main.py

ARG USER_ID
ARG USER_GID
ARG HOST_DOCKER_GID
RUN groupadd -g ${USER_GID} -o sc-user && \
    groupadd -g ${HOST_DOCKER_GID} -o sc-docker && \
    useradd -m -u ${USER_ID} -g ${USER_GID} -G sc-docker -o sc-user

USER sc-user
ENV HOME /home/sc-user

WORKDIR /smart-camera

ENTRYPOINT ["python3", "compilation/main.py"]
