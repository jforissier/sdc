all: application-image-save

.PHONY: application

application: out/.stamp_application

application-image-save: out/sc-application.tar.gz

application-image-push: FORCE out/.stamp_application | $(builder-image)
	$(call run-builder,docker tag sc-application $(DOCKER_NAMESPACE)/sc-application)
	$(call run-builder,docker push $(DOCKER_NAMESPACE)/sc-application)

images-push: application-image-push

out/.stamp_application: application/config/config.json | host-aarch64-registered $(builder-image)
	$(call run-builder,docker buildx build $(DOCKER_BUILD_OPTS) --platform aarch64 -t sc-application application)
	$(q)touch $@

out/sc-application.tar.gz: out/.stamp_application | $(builder-image)
	@$(quiet) "  DOCKSAV $@"
	$(q)$(call run-builder,bash -c 'docker save sc-application | pigz >$@')

staging-gg-app = out/staging/greengrassv2/SmartCameraApplication

greengrass-application-component: $(staging-gg-app)/zip-build/SmartCameraApplication.zip

$(staging-gg-app)/zip-build/SmartCameraApplication.zip: $(staging-gg-app)/sc-application.tar.gz \
				$(staging-gg-app)/gdk-config.json \
				$(staging-gg-app)/recipe.yaml | $(builder-image)
	@$(quiet) "  BUILD   $@"
	$(q)$(call run-builder,bash -c 'cd $(staging-gg-app) && gdk component build')

$(staging-gg-app)/sc-application.tar.gz: out/sc-application.tar.gz
	$(call cp-file)

$(staging-gg-app)/gdk-config.json: application/greengrassv2/SmartCameraApplication/gdk-config.json
	$(call cp-file)

$(staging-gg-app)/recipe.yaml: application/greengrassv2/SmartCameraApplication/recipe.yaml
	$(call cp-file)

greengrass-application-component-publish: $(staging-gg-app)/zip-build/SmartCameraApplication.zip \
				out/.stamp_greengrass-attach-role-policy | $(builder-image)
	@$(quiet) "  PUBLISH $(notdir $<)"
	$(q)$(call run-builder,./aws/gdk_component_publish.sh $(staging-gg-app))

greengrass-components: greengrass-application-component

greengrass-components-publish: greengrass-application-component-publish
