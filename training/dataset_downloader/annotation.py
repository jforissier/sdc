import json
import cv2

def annotate():
    with open('out/ann.json') as json_file:
        ann_dict = json.load(json_file)

    fp = open("out/dataset/train.txt","w")

    for k in ann_dict.keys():
        a = ann_dict[k]
        img_file = k + ".jpg"
        fp.write("out/dataset/"+img_file + " ")
        for l in a:
            x1,y1,x2,y2,id = l[0],l[1],l[2],l[3],l[4]
            fp.write(str(x1) + "," + str(y1) + "," + str(x2) + "," + str(y2) + "," +str(id) + " ")
        fp.write("\n")



    
