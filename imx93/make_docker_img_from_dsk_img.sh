#!/bin/bash
#
# Must be run as root in the sc-builder container image (which is privileged)
# because it uses losetup and mount/umount to access the BSP image
# The script does NOT create any file owned by root in the host filesystem
# however. A temporary directory in the container is used to receive the root
# FS extracted from the BSP image.

function cleanup() {
	if [ -d ${MNT} ]; then
		umount ${MNT} || :
		rmdir ${MNT}
	fi
        if [ "${LO}" != "" ]; then
                losetup -d ${LO}
        fi
}

set -ev
trap cleanup EXIT

PART=$1
IMG=$2
STAGING=/tmp/bsp-rootfs

LO=$(losetup -f)
losetup -P ${LO} ${IMG}
MNT=$(mktemp -d -p out)
mount ${LO}p${PART} ${MNT}
rsync -azh ${MNT}/{bin,dev,etc,home,lib,lost+found,run,sbin,sys,tmp,usr,var} ${STAGING}
cp imx93/Dockerfile ${STAGING}
docker buildx build --no-cache --platform aarch64 -t sc-imx93-bsp ${STAGING}
