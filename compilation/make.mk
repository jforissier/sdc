c := compilation

.PHONY: compilation

ifeq ($(COMPILATION_MODE),off)

compilation:
	@echo "Info: model compilation is disabled (COMPILATION_MODE=$(COMPILATION_MODE))"

else ifeq ($(COMPILATION_MODE),local)

include $(c)/local.mk

else ifeq ($(COMPILATION_MODE), aws)

include $(c)/aws.mk

else ifeq ($(COMPILATION_MODE), prebuilt)

include $(c)/prebuilt.mk

else

$(error Invalid value for COMPILATION_MODE)

endif # COMPILATION_MODE
