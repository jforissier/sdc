#!/bin/sh
#
# Run the ethosu-test.py script in the standard Python3 Docker image
# This way we don't need to bother about dependencies (one example: the
# fcntl module is not available in TRS)
docker run -it --rm --privileged -v "$PWD":/usr/src/myapp -w /usr/src/myapp python:3 python ethosu-test.py
