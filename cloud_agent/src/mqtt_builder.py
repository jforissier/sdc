import argparse
from awscrt import io, http, auth
from awsiot import mqtt_connection_builder


class MQTT_builder:
    def __init__(self, description) -> None:
        self.parser = argparse.ArgumentParser(
                description="Send and receive messages through an MQTT "
                            "connection.")
        self.commands = {}
        self.parsed_commands = None

    def build_mqtt_connection(self, on_connection_interrupted,
                              on_connection_resumed, endpoint, port, cert_path,
                              pri_key, ca_path, client_id):
        mqtt_connection = mqtt_connection_builder.mtls_from_path(
            endpoint=endpoint,
            port=port,
            cert_filepath=cert_path,
            pri_key_filepath=pri_key,
            ca_filepath=ca_path,
            on_connection_interrupted=on_connection_interrupted,
            on_connection_resumed=on_connection_resumed,
            client_id=client_id,
            clean_session=False,
            keep_alive_secs=30)
        return mqtt_connection


def on_connection_interrupted(connection, error, **kwargs):
    print(f"Connection interrupted. error: {error}")


def on_connection_resumed(connection, return_code, session_present, **kwargs):
    print(f"Connection resumed. return_code: {return_code} "
          "session_present: {session_present}")
